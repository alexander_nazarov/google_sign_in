# README #

Simple Google Sign-In with Django.

### How do I get set up? ###

* clone repo
```git clone https://bitbucket.org/alexander_nazarov/google_sign_in.git```
* go to project
```cd google_sign_in```
* install python 3.6
* create env (e.g. ```virtualenv -p /usr/local/bin/python3.6 .env```)
* activate env 
```source .env/bin/activate```
* ```pip install -U setuptools```
* install requirements
```pip install -r requirements.txt```

* ```cd google_app```
* make migrations and run
```
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

* go to *http://localhost:8000*
