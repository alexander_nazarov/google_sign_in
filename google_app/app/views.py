from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render


@login_required
def index(request):
    return render(request, 'index.html')


def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/')
